namespace Constants {
   public const string DATADIR = "/usr/share";
   public const string PKGDATADIR = "/usr/share/valistic";
   public const string GETTEXT_PACKAGE = "valistic";
   public const string RELEASE_NAME = "Danna";
   public const string APP_VERSION = "0.0.1.1";
   public const string VERSION_INFO = "debug";
   public const string INSTALL_PREFIX = "/usr";
}
