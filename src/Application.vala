public class ValisticApp : Gtk.Application{
	
	public ValisticApp(){
		Object (application_id: "co.douglasbrunal.valistic");
	}

	protected override void startup(){
		base.startup();
		Gtk.Window.set_default_icon_name(Valistic.ICON_NAME);
	}

	public override void activate(){
		new_window();
	}

	private void new_window(File? open_file = null){
		var window = new Window(this);
		add_window(window);

		if (open_file != null) {
			window.use_file(open_file);
		}

		window.present();
	}

	public void show_about (Window? parent = null){
		var dialog = new AboutDialog();
		if (parent != null) {
	//		dialog.set_transfer_for(parent);
		}
		dialog.run();
	}

}