namespace Valistic {
	const string APP_NAME = "Valistic";
	const string APP_SYSTEM_NAME = "valistic-ide";
	const string APP_ID = "co.douglasbrunal.co";
	const string APP_VERSION = Constants.APP_VERSION;
	const string ICON_NAME = "valistic-ide";
}