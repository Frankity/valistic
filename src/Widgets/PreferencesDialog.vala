public class PreferencesDialog : Gtk.Dialog {
    private enum StylesheetState {
        NONE,
        DEFAULT,
        CUSTOM
    }

    private Preferences prefs;
    private Gtk.FontButton font_btn;

    private Gtk.ListStore schemes_store;
    private Gtk.TreeIter schemes_iter;
    private Gtk.ComboBox scheme_box;

    private Gtk.Switch dark_theme_switch;
    private Gtk.Switch hightligh_cur_line;
    private Gtk.Switch show_line_numbers;
    private Gtk.Switch show_folder_view;

    private Gtk.ListStore stylesheet_store;
    private Gtk.ComboBox stylesheet_box;

    private Gtk.Switch syntax_highlighting_switch;

    private Gtk.Revealer csb_revealer;
    
    private Gtk.TextView text_view;

    public PreferencesDialog (Window parent, Preferences prefs) {
        this.set_transient_for (parent);
        this.prefs = prefs;

        setup_ui ();
        setup_events ();
    }

    private void setup_ui () {
        this.title = _("Preferences");
        this.window_position = Gtk.WindowPosition.CENTER;
        this.set_modal (true);
        this.border_width = 10;

        var main_layout = new Gtk.Box (Gtk.Orientation.VERTICAL, 10);

        var editor_prefs = get_editor_prefs ();
        var features_prefs = get_features_prefs();
        var buildrun_prefs = get_buildrun_prefs();

        var stack = new Gtk.Stack ();
        stack.halign = Gtk.Align.CENTER;
        var switcher = new Gtk.StackSwitcher ();

        switcher.halign = Gtk.Align.CENTER;
        switcher.set_stack (stack);

        stack.add_titled (editor_prefs, "editor-prefs", _("Editor"));
        stack.add_titled (features_prefs, "features_prefs", _("Features"));
        stack.add_titled (buildrun_prefs, "buildrun_prefs", _("Build & Run"));

        main_layout.pack_start (switcher);
        main_layout.pack_start (stack);

        get_content_area ().add (main_layout);
    }

    private void setup_events () {
        font_btn.font_set.connect (() => {
            unowned string name = font_btn.get_font_name ();
            prefs.editor_font = name;
        });

        scheme_box.changed.connect(() => {
            Value box_val;
            scheme_box.get_active_iter (out schemes_iter);
            schemes_store.get_value (schemes_iter, 0, out box_val);

            var scheme_id = (string) box_val;
            prefs.editor_scheme = scheme_id;
        });

        // can't use Gtk.Switch::state as it's not supported on gtk+-3.10
        dark_theme_switch.notify["active"].connect(() => {
            prefs.prefer_dark_theme = dark_theme_switch.get_active ();
        });

        hightligh_cur_line.notify["active"].connect(() => {
            prefs.hightlight_current_line = hightligh_cur_line.get_active ();
        });

        show_line_numbers.notify["active"].connect(() => {
            prefs.line_numbers = show_line_numbers.get_active ();
        });

        show_folder_view.notify["active"].connect(() => {
            prefs.folder_view = show_folder_view.get_active ();
        });

        stylesheet_box.changed.connect (() => {
            Gtk.TreeIter iter;
            stylesheet_box.get_active_iter (out iter);

            GLib.Value state_value;
            stylesheet_store.get_value (iter, 1, out state_value);
            StylesheetState state = (StylesheetState) state_value.get_int ();

            switch (state) {
            case StylesheetState.NONE:
                prefs.render_stylesheet = false;
                csb_revealer.set_reveal_child (false);
                break;

            case StylesheetState.CUSTOM:
                csb_revealer.set_reveal_child (true);
                break;

            case StylesheetState.DEFAULT:
                prefs.render_stylesheet = true;
                prefs.render_stylesheet_uri = "";
                csb_revealer.set_reveal_child (false);
                break;
            }
        });

        // can't use Gtk.Switch::state as it's not supported on gtk+-3.10
        syntax_highlighting_switch.notify["active"].connect((state) => {
            prefs.render_syntax_highlighting = syntax_highlighting_switch.get_active ();
        });
    }

    private Gtk.Grid get_features_prefs(){
        var layout = new Gtk.Grid();
        layout.margin = 10;
        layout.row_spacing = 12;
        layout.column_spacing = 9;
        int row = 0;

        var highlight_line_label = new Gtk.Label (_("Highlight current line"));
        hightligh_cur_line = new Gtk.Switch ();
        hightligh_cur_line.active = prefs.hightlight_current_line;

        layout.attach (highlight_line_label, 0, row, 1, 1);
        layout.attach_next_to (hightligh_cur_line, highlight_line_label, Gtk.PositionType.RIGHT, 1, 1);
        row++;

        var show_line_numbers_label = new Gtk.Label (_("Show Line Numbers"));
        show_line_numbers = new Gtk.Switch ();
        show_line_numbers.active = prefs.line_numbers;

        layout.attach (show_line_numbers_label, 0, row, 1, 1);
        layout.attach_next_to (show_line_numbers, show_line_numbers_label, Gtk.PositionType.RIGHT, 1, 1);
        row++;
        
        var show_folder_view_label = new Gtk.Label (_("Show Folder View"));
        show_folder_view = new Gtk.Switch ();
        show_folder_view.active = prefs.folder_view;

        layout.attach (show_folder_view_label, 0, row, 1, 1);
        layout.attach_next_to (show_folder_view, show_folder_view_label, Gtk.PositionType.RIGHT, 1, 1);
        
        return layout;
    }

    private Gtk.Grid get_editor_prefs () {
        var layout = new Gtk.Grid ();
        layout.margin = 10;
        layout.row_spacing = 12;
        layout.column_spacing = 9;
        int row = 0;

        font_btn = new Gtk.FontButton ();
        font_btn.use_font = true;
        font_btn.use_size = true;

        if (prefs.editor_font != "") {
            font_btn.set_font_name (prefs.editor_font);
        }

        var font_label = new Gtk.Label.with_mnemonic (_("Editor _font:"));
        font_label.mnemonic_widget = font_btn;

        layout.attach (font_label, 0, row, 1, 1);
        layout.attach_next_to (font_btn, font_label, Gtk.PositionType.RIGHT, 1, 1);
        row++;

        schemes_store = new Gtk.ListStore (2, typeof (string), typeof (string));

        scheme_box = new Gtk.ComboBox.with_model (schemes_store);
        var scheme_renderer = new Gtk.CellRendererText ();
        scheme_box.pack_start (scheme_renderer, true);
        scheme_box.add_attribute (scheme_renderer, "text", 1);

        var schemes = this.get_source_schemes ();
        int i = 0;
        schemes_iter = {};
        foreach (var scheme in schemes) {
            schemes_store.append (out schemes_iter);
            schemes_store.set (schemes_iter, 0, scheme.id, 1, scheme.name);

            if (scheme.id == prefs.editor_scheme) {
                scheme_box.active = i;
            }

            i++;
        }

        var scheme_label = new Gtk.Label.with_mnemonic (_("Editor _theme:"));
        scheme_label.mnemonic_widget = scheme_box;

        layout.attach (scheme_label, 0, row, 1, 1);
        layout.attach_next_to (scheme_box, scheme_label, Gtk.PositionType.RIGHT, 1, 1);
        row++;

        // Autosave
        // Dark theme
        var dark_theme_label = new Gtk.Label (_("Enable dark theme"));
        dark_theme_switch = new Gtk.Switch ();
        dark_theme_switch.active = prefs.prefer_dark_theme;

        layout.attach (dark_theme_label, 0, row, 1, 1);
        layout.attach_next_to (dark_theme_switch, dark_theme_label, Gtk.PositionType.RIGHT, 1, 1);
        row++;


        return layout;
    }

    private Gtk.Box get_buildrun_prefs () {

        Gtk.Box box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);


        Gtk.Entry entry = new Gtk.Entry ();

        // Add a default-text:
        entry.set_placeholder_text ("--pkg gtk+-3.0 --pkg gdk-3.0 --pkg granite etc...");

        // Add a delete-button:
        entry.set_icon_from_icon_name (Gtk.EntryIconPosition.SECONDARY, "edit-clear");
        entry.icon_press.connect ((pos, event) => {
            if (pos == Gtk.EntryIconPosition.SECONDARY) {
                entry.set_text ("");
            }
        });

        box.pack_start (new Gtk.Label("Compilation args"), false, false, 0);
        box.pack_start (new Gtk.Label(""), false, false, 0);
        box.pack_start (entry, false, false, 0);

        return box;
    }


   

    private Gtk.SourceStyleScheme[] get_source_schemes () {
        var style_manager = Gtk.SourceStyleSchemeManager.get_default ();
        unowned string[] scheme_ids = style_manager.get_scheme_ids ();
        Gtk.SourceStyleScheme[] schemes = {};

        foreach (string id in scheme_ids) {
            schemes += style_manager.get_scheme (id);
        }
        return schemes;
    }
}