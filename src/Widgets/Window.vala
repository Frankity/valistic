using Gdk;

public class Window : Gtk.ApplicationWindow{
	private ValisticApp app;
    private DocumentView doc;
	private FileFoldersView fview;
	private Preferences prefs;
	private SavedState saved_state;
	private Gtk.HeaderBar? headerbar = null;
    private Gtk.Clipboard clipboard;
    private Gtk.Paned box;

    private int folder_status;

	private File? _currfile;
	
	private File? curfile{
		get {
			return _currfile;
		}
		set{
			_currfile = value;
			add_fn_title();
		}
		default = null;
	}

    private bool file_modified = false;
    private FileMonitor? file_monitor = null;

    public signal void updated();

    public Window(ValisticApp app){
        this.app  = app;
        add_action_entries (win_actions, this);

        setup_prefs();
        setup_ui();
        setup_file_monitor();
        setup_events();
        prefs.load ();
    }


    private void update_state () {
        file_modified = true;
    }

    private const GLib.ActionEntry win_actions[] =
    {
        { "new", new_action },
        { "open", open_action },
        { "save", save_action },

        { "print", export_print_action },

        { "preferences", preferences_action },
        { "buildrun", compile_run },
        { "about", about_action }
    };

	private void add_fn_title () {
        string title = Valistic.APP_NAME;

        if (curfile != null) {
            var file = (!) curfile;
            title = "%s - %s".printf (file.get_basename (),
                                          Valistic.APP_NAME);
        }
        if (headerbar != null)
            headerbar.set_title (title);
        else
            set_title (title);
    }

        public void use_file (File? file, bool should_monitor = true) {
        if (file != null) {
            FileHandler.load_content_from_file.begin (file, (obj, res) => {
                doc.set_text (FileHandler.load_content_from_file.end (res), true);
                if (should_monitor) {
                    setup_file_monitor ();
                }
            });
        }
        curfile = file;
        file_modified = false;
    }

    private void setup_prefs () {
        prefs = new Preferences ();
        prefs.load ();
        prefs.notify["editor-font"].connect ((s, p) => {
            if (prefs.editor_font == "") {
                var isettings = new GLib.Settings ("org.gnome.desktop.interface");
                prefs.editor_font = isettings.get_string ("monospace-font-name");
            }
            doc.set_font (prefs.editor_font);
        });

        prefs.notify["editor-scheme"].connect ((s, p) => {
            doc.set_scheme (prefs.editor_scheme);
        });

        prefs.notify["line-numbers"].connect ((s, p) => {
            doc.show_lines (prefs.line_numbers);
        });

        prefs.notify["hightlight-current-line"].connect ((s, p) => {
            doc.high_line (prefs.hightlight_current_line);
        });

        prefs.notify["folder-view"].connect ((s, p) => {
            if (prefs.folder_view == true) {
                folder_status = 1;
            }else {
                folder_status = 0;
            }
        });

        prefs.notify["prefer-dark-theme"].connect ((s, p) => {
            Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", prefs.prefer_dark_theme);
        });

        
    }


    private void setup_file_monitor () {
        if (file_monitor != null) {
            file_monitor.cancel ();
        }

        if (curfile != null) {
            try {
                file_monitor = curfile.monitor_file (
                    FileMonitorFlags.NONE);
            } catch (Error e) {
                warning ("Could not monitor file");
            }
            file_monitor.changed.connect (file_changed_event);
        }
    }

    private void setup_events () {
        this.key_press_event.connect (key_pressed);
        doc.changed.connect (update_state);
    }

        private void file_changed_event (File old_file, File? new_file,
                                     FileMonitorEvent event_type) {
        switch (event_type) {
        case FileMonitorEvent.CHANGED:
            use_file (old_file, false);
            break;

        case FileMonitorEvent.MOVED:
            use_file (new_file);
            break;
        }
    }

    public bool key_pressed (Gdk.EventKey ev) {
        bool handled_event = false;
        bool ctrl_pressed = modifier_pressed (ev,
                                              Gdk.ModifierType.CONTROL_MASK);

        switch (ev.keyval) {
        case Gdk.Key.C:
            if (ctrl_pressed) {
                handled_event = true;
            }
            break;

        case Gdk.Key.o:
            if (ctrl_pressed) {
                handled_event = true;
                open_action ();
            }
            break;
        case Gdk.Key.y:
            if (ctrl_pressed) {
                handled_event = true;
               new FileFoldersView().get_dir("/home/am/");
            }
            break;

        case Gdk.Key.n:
            if (ctrl_pressed) {
                handled_event = true;
                new_action ();
            }
            break;

        case Gdk.Key.s:
            if (ctrl_pressed) {
                handled_event = true;
                save_action ();
            }
            break;

        case Gdk.Key.q:
            if (ctrl_pressed) {
                handled_event = true;
                close_action ();
            }
            break;

        case Gdk.Key.p:
            if (ctrl_pressed) {
                handled_event = true;
                var dialog = new PreferencesDialog(this, this.prefs);
                dialog.show_all ();
            }
            break;

        case Gdk.Key.h:
            if (ctrl_pressed) {
                handled_event = true;
                
                if (prefs.hightlight_current_line){
                    doc.high_line(false);
                    prefs.hightlight_current_line = false;
                }else {
                    doc.high_line(true);
                    prefs.hightlight_current_line = true;
                }

            }
            break;

        case Gdk.Key.k:
            if (ctrl_pressed) {
                handled_event = true;
                
                if (prefs.folder_view){
                    box.position = 0;
                    prefs.folder_view = false;
                }else {
                    box.position = 150;
                    prefs.folder_view = true;
                }

            }
            break;

        case Gdk.Key.l:
            if (ctrl_pressed) {
                handled_event = true;
                
                if (prefs.line_numbers){
                    doc.show_lines(false);
                    prefs.line_numbers = false;
                }else {
                    doc.show_lines(true);
                    prefs.line_numbers = true;
                }

            }
            break;

        case Gdk.Key.F11:
            // get Gdk.Window's state
            var window_state = get_window ().get_state ();

            if ((window_state & Gdk.WindowState.FULLSCREEN) != 0) {
                unfullscreen ();
            } else {
                fullscreen ();
            }
            break;
        case Gdk.Key.F1:
            app.show_about (this);

            break;

        case Gdk.Key.F5:
            compile_run();
            break;
        }
        
        return handled_event;

    }


    private bool modifier_pressed (Gdk.EventKey event,
                                   Gdk.ModifierType modifier) {

        return (event.state & modifier)  == modifier;
    }

    private void compile_run(){
            if (curfile == null) {
                Gtk.MessageDialog msg = new Gtk.MessageDialog(this, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "There's no file to Debug / Run!");
                    msg.response.connect ((response_id) => {
                    switch (response_id) {
                        case Gtk.ResponseType.OK:
                            stdout.puts ("Ok\n");
                            break;
                        case Gtk.ResponseType.DELETE_EVENT:
                            stdout.puts ("Delete\n");
                            break;
                    }

                    msg.destroy();
                });
                msg.show ();
            }else{
                try {
                string ls_stdout;
                string ls_stderr;
                int ls_status;
                string f_ds = curfile.get_path().replace(curfile.get_basename(), "");
                GLib.Process.spawn_command_line_sync("valac -d " + f_ds + " --pkg gtk+-3.0 --pkg gdk-3.0 " + curfile.get_path(),
                                                out ls_stdout,
                                                out ls_stderr,
                                                out ls_status);
                int st = curfile.get_path().length;   
                string res = curfile.get_path().to_string().splice(st - 5, st);
                GLib.Process.spawn_command_line_async(res);
                    stdout.printf ("file_to_run: " + res);

                    // Output: <File list>
                    stdout.printf ("stdout:\n");
                    // Output: ````
                    stdout.puts (ls_stdout);
                    stdout.printf ("stderr:\n");
                    stdout.puts (ls_stderr);
                    // Output: ``0``
                    stdout.printf ("status: %d\n", ls_status);
                } catch (SpawnError e) {
                    stdout.printf ("Error: %s\n", e.message);
                }
            }

    }

    private void new_action () {
        if (file_modified) {
            var dialog = new UnsavedChangesDialog.for_close_file (this);
            var result = dialog.run ();
            dialog.destroy ();

            if (result == UnsavedChangesResult.CANCEL) {
                return;
            } else if (result == UnsavedChangesResult.SAVE) {
                save_action ();
            } else {
                // the user doesn't care about the file,
                // close it anyway
                reset_file ();
            }
        }
        reset_file ();
    }

    private void open_action () {
        var new_file = get_file_from_user (DialogType.MARKDOWN_IN);
        use_file (new_file);
    }

    private void save_action () {
        if (curfile == null) {
            var file = get_file_from_user (DialogType.MARKDOWN_OUT);

            if (file == null) {
                return;
            } else {
                curfile = file;
            }
        }

        try {
            FileHandler.write_file (curfile, doc.get_text ());
            file_modified = false;
        } catch (Error e) {
            warning ("%s: %s", e.message, curfile.get_basename ());
        }
    }

    private void close_action () {
        close ();
    }


    private void export_print_action () {
       
    }

    private void preferences_action () {
        var dialog = new PreferencesDialog(this, this.prefs);
        dialog.show_all ();
    }

    private void about_action () {
        app.show_about (this);
    }
    
    private void load_window_state () {
        saved_state = new SavedState ();
        saved_state.load ();

        int window_width = saved_state.window_width;
        int window_height = saved_state.window_height;
        WindowState state = saved_state.window_state;
        set_default_size (window_width, window_height);

        if (state == WindowState.MAXIMIZED) {
            maximize ();
        } else if (state == WindowState.FULLSCREEN) {
            fullscreen ();
        }
        int x = saved_state.opening_x;
        int y = saved_state.opening_y;

        move (x, y);
    }

private void save_window_state () {
        // get Gdk.Window then get it's state
        var window_state = get_window ().get_state ();

        if ((window_state & Gdk.WindowState.MAXIMIZED) != 0) {
            saved_state.window_state = WindowState.MAXIMIZED;
        } else if ((window_state & Gdk.WindowState.FULLSCREEN) != 0) {
            saved_state.window_state = WindowState.FULLSCREEN;
        } else {
            saved_state.window_state = WindowState.NORMAL;
        }

        int width, height;
        get_size (out width, out height);
        saved_state.window_width = width;
        saved_state.window_height = height;

        int x, y;
        get_position (out x, out y);
        saved_state.opening_x = x;
        saved_state.opening_y = y;
    }

        public override bool delete_event (Gdk.EventAny ev) {
        var dont_quit = false;

        if (file_modified) {
            var d = new UnsavedChangesDialog.for_quit (this);
            var result = d.run ();

            switch (result) {
            case UnsavedChangesResult.QUIT:
                dont_quit = false;
                break;

            // anything other than quit means cancel
            case UnsavedChangesResult.CANCEL:
            default:
                dont_quit = true;
                break;
            }
            d.destroy ();
        }

        // save state anyway
        save_window_state ();

        return dont_quit;
    }

    private File? get_file_from_user (DialogType dtype) {
        File? result = null;

        string title = "";
        Gtk.FileChooserAction chooser_action = Gtk.FileChooserAction.SAVE;
        string accept_button_label = "";
        List<Gtk.FileFilter> filters = new List<Gtk.FileFilter> ();

        switch (dtype) {
        case DialogType.MARKDOWN_OUT:
            title =  _("Select destination markdown file");
            chooser_action = Gtk.FileChooserAction.SAVE;
            accept_button_label = _("Save");

            filters.append (get_vala_filter ());
            break;

        case DialogType.MARKDOWN_IN:
            title = _("Select markdown file to open");
            chooser_action = Gtk.FileChooserAction.OPEN;
            accept_button_label = _("Open");

            filters.append (get_vala_filter ());
            break;

        case DialogType.HTML_OUT:
            title =  _("Select destination HTML file");
            chooser_action = Gtk.FileChooserAction.SAVE;
            accept_button_label = _("Save");

            var html_filter = new Gtk.FileFilter ();
            html_filter.set_filter_name ("HTML File");

            html_filter.add_mime_type ("text/plain");
            html_filter.add_mime_type ("text/html");

            html_filter.add_pattern ("*.html");
            html_filter.add_pattern ("*.htm");

            filters.append (html_filter);
            break;

        case DialogType.PDF_OUT:
            title =  _("Select destination PDF file");
            chooser_action = Gtk.FileChooserAction.SAVE;
            accept_button_label = _("Save");

            var pdf_filter = new Gtk.FileFilter ();
            pdf_filter.set_filter_name ("PDF File");

            pdf_filter.add_mime_type ("application/pdf");
            pdf_filter.add_pattern ("*.pdf");

            filters.append (pdf_filter);
            break;

        }

        var all_filter = new Gtk.FileFilter ();
        all_filter.set_filter_name ("All Files");
        all_filter.add_pattern ("*");

        filters.append (all_filter);

        var dialog = new Gtk.FileChooserDialog (
            title,
            this,
            chooser_action,
            _("_Cancel"), Gtk.ResponseType.CANCEL,
            accept_button_label, Gtk.ResponseType.ACCEPT);


        filters.@foreach ((filter) => {
            dialog.add_filter (filter);
        });

        if (dialog.run () == Gtk.ResponseType.ACCEPT) {
            result = dialog.get_file ();
        }

        dialog.close ();

        return result;
    }

    private Gtk.FileFilter get_vala_filter () {
        var mk_filter = new Gtk.FileFilter ();
        mk_filter.set_filter_name ("Vala Files");

        mk_filter.add_mime_type ("text/plain");
        mk_filter.add_mime_type ("text/x-vala");

        mk_filter.add_pattern ("*.vala");
        mk_filter.add_pattern ("*.txt");
        mk_filter.add_pattern ("*.cs");

        return mk_filter;
    }


    public void reset_file () {
        file_monitor.cancel ();
        file_monitor = null;

        file_modified = false;
        curfile = null;
        doc.reset ();

         
    }

    private enum DialogType {
        MARKDOWN_OUT,
        MARKDOWN_IN,
        HTML_OUT,
        PDF_OUT
    }

     private void setup_ui () {
        load_window_state ();

        window_position = Gtk.WindowPosition.CENTER;
        set_hide_titlebar_when_maximized (false);
        clipboard = Gtk.Clipboard.get_for_display(get_display(), Gdk.SELECTION_CLIPBOARD);


        box = new Gtk.Paned (Gtk.Orientation.HORIZONTAL);
        //box.expand = true;

        bool use_headerbar = true;
        Gtk.Builder builder;
        if (use_headerbar) {
            builder = new Gtk.Builder.from_resource ("/co/douglasbrunal/valistic/ui/headerbar.ui");
            headerbar = (Gtk.HeaderBar) builder.get_object ("headerbar");
            headerbar.set_title (Valistic.APP_NAME);
            headerbar.set_subtitle ("An epic code editor for Elementary Os");

            set_titlebar ((Gtk.Widget) headerbar);
            add (box);
        } else {
            builder = new Gtk.Builder.from_resource ("/co/douglasbrunal/valistic/ui/toolbar.ui");
            Gtk.Box layout = (Gtk.Box) builder.get_object ("layout");

            layout.pack_start (box, false);
            add (layout);
        }

        Gtk.IconTheme icon_theme = Gtk.IconTheme.get_default ();
        var pref_menu = (Gtk.MenuToolButton) builder.get_object ("prefMenu");
        var export_menu = (Gtk.MenuToolButton) builder.get_object ("exportMenu");

        Gtk.Image menu_icon = (Gtk.Image) builder.get_object ("menu-icon");
        Gtk.Image export_icon = (Gtk.Image) builder.get_object ("export-icon");

        if (!icon_theme.has_icon ("open-menu")) {
            var alternate_icon = "preferences-system";
            if (!icon_theme.has_icon (alternate_icon)) {
                alternate_icon = "gtk-preferences";
            }
            menu_icon.set_from_icon_name (alternate_icon, Gtk.IconSize.LARGE_TOOLBAR);
            pref_menu.icon_name = alternate_icon;
        }
        if (!icon_theme.has_icon ("document-export")) {
            export_icon.set_from_icon_name ("document-revert-rtl", Gtk.IconSize.LARGE_TOOLBAR);
            export_menu.icon_name = "document-revert-rtl";
        }

        int width;
        get_size (out width, null);
        saved_state = new SavedState ();
        saved_state.load ();

       box.set_position (width - width + 150);
        int b;

        if (folder_status == 1) {
            b  = 1;
        }else{
            b = 1;
        }

        //box.set_position(1);

        doc = new DocumentView ();
        fview = new FileFoldersView();

        box.pack1 (fview,false,false);
        box.pack2 (doc,false,false);

        doc.give_focus ();

        box.show_all ();
    }



}


