public class AboutDialog : Gtk.AboutDialog {

    public AboutDialog () {
        this.set_default_size (200, 500);
        this.get_content_area ().margin = 10;
        this.title = "About %s".printf (Valistic.APP_NAME);
        setup_content ();
        this.response.connect (response_handler);
    }

    
    private void setup_content () {
        program_name = Valistic.APP_NAME;
        logo_icon_name = Valistic.ICON_NAME;
        
        comments = "A Vala editor that allows you to easily make your own " +
                   "Vala applications.";

        website = "http://gitlab.com/Frankity/valistic";
        version = Valistic.APP_VERSION;

        artists = {"Douglas Brunal", "Jorge Brunal"};
        
        authors = { "<a href='https://douglasbrunal.co'>Douglas Brunal</a> as Main Developer",
                    "<a href='https://jorgebrunal.co'>Jorge Brunal</a> The Improvements guy"};
        copyright = "Copyright © 2016-2017 Douglas Brunal";
        license_type = Gtk.License.GPL_3_0;
        wrap_license = true;
    }

    private void response_handler (int response) {
        if (response == Gtk.ResponseType.DELETE_EVENT) {
            this.destroy ();
        }   
    }
}
