public class FileFoldersView : Gtk.ScrolledWindow {

	private Gtk.ListStore model;

	public FileFoldersView(){
			this.model = new Gtk.ListStore(1, typeof(string));
			
			var view = new Gtk.TreeView.with_model(this.model);
	        Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	        view.insert_column_with_attributes (-1, "Name", cell, "text", 0,null);
	        
	        Gtk.TreeIter root;
	        Gtk.TreeIter category_iter;
	        Gtk.TreeIter product_iter;

	        view.set_headers_visible(false);

	        var scroll = new Gtk.ScrolledWindow (null, null);
    	    scroll.add (view);
        	add (scroll);
	}

	public void get_dir(string dir){
		dir_walk(dir);
	}

	public async void dir_walk(string dir){
		var ds = File.new_for_path(dir);
		try {
			var e = yield ds.enumerate_children_async(FileAttribute.STANDARD_NAME, 0, Priority.DEFAULT);
			while (true) {
				var files = yield e.next_files_async(10, Priority.DEFAULT);
				if (files == null) {
					break;
				}

				foreach (var info in files) {
					Gtk.TreeIter iter;
					this.model.append(out iter);
					this.model.set(iter, 0, info.get_name());
				}

			}
		} catch (Error e) {
			stderr.printf ("Error: list_files failed: %s\n", e.message);
		}

		
	}

}

