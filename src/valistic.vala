using Gdk;
using Gtk;

public class Granite.Demo : Granite.Application {
    Gtk.Window window;
    Gtk.Paned main_paned;
    Gtk.Stack main_stack;
    Gtk.Button home_button;
    Gtk.Button save_button;
    //Gtk.Button add_button;
    //Gtk.Button new_button;
    //Gtk.Button run_buton;
    Gtk.Button opt_button;
    //Gtk.TextView new_task_entry;
    Gtk.TextView text_area_view;
    private string? fe_name;
ssss
    construct {
        application_id = "co.douglasbrunal.valistic";
        flags = ApplicationFlags.FLAGS_NONE;

        program_name = "Valistic";
        app_years = "2017";

        build_version = "0.0.0.2";
        app_icon = "text-x-generic";
        main_url = "http://douglasbrunal.co/blog/valistic";
        bug_url = "https://bugs.launchpad.net/granite";
        help_url = "https://answers.launchpad.net/granite";
        translate_url = "https://translations.launchpad.net/granite";

        about_documenters = { null };
        about_artists = { "Douglas Brunal <frankithy@gmail.com>" };
        about_authors = {
            "Douglas Brunal <frankithy@gmail.com",
            "Jorge Brunal <diniremix@gmail.com"
        };

        about_comments = "An epic code editor for Elementary Os";
        about_translators = "translator-credits";
        about_license_type = Gtk.License.GPL_3_0;
    }

    public override void activate () {
        window = new Gtk.Window ();
        window.window_position = Gtk.WindowPosition.CENTER;
        add_window (window);

        main_stack = new Gtk.Stack ();
        main_stack.transition_type = Gtk.StackTransitionType.SLIDE_LEFT_RIGHT;
        main_paned = new Gtk.Paned (Gtk.Orientation.HORIZONTAL);

        create_headerbar ();
        create_wellcome();
        //create_hint();

        create_dynamictab();
        showtxtarea();

        window.add (main_stack);
        window.maximize();
        window.show_all ();
        home_button.hide ();
        save_button.hide ();
    }


    private void create_wellcome(){
        var welcome = new Granite.Widgets.Welcome ("To start choose an option.", "Create epic and beautiful applications to your loved OS.");
        welcome.append ("document-open", "Open a file", "Re open an older file.");
        welcome.append ("document-new", "Creat a new file", "Create a new file and start working on it.");
        welcome.append ("folder-open", "Open a folder", "Open a folder and create a heracy structure.");
        welcome.append ("emblem-shared", "Share a file", ("Share a piece of code in pastebin."));
        welcome.activated.connect ((index) => {
            switch (index) {
                case 0:
                    open_a_file();
                    break;
                case 1:
                    home_button.show ();
                    save_button.show ();
                    main_stack.set_visible_child_name ("sourcelist");
                    break;
                case 2:
                    home_button.show ();
                    save_button.show ();
                    main_stack.set_visible_child_name ("sourcelist");
                    break;
                case 3:
                    home_button.show ();
                    save_button.show ();
                    main_stack.set_visible_child_name ("dynamictab");
                    break;
                        }
        });
        var scrolled = new Gtk.ScrolledWindow (null, null);
        scrolled.add (welcome);
        main_stack.add_named (scrolled, "welcome");
    }
   

    private void create_headerbar () {
        var headerbar = new Gtk.HeaderBar ();
        headerbar.title = "Valistic " + build_version;
        headerbar.subtitle = "An epic code editor for Elementary Os";
        headerbar.show_close_button = true;

        var about_button = new Gtk.Button.from_icon_name ("help-about", Gtk.IconSize.LARGE_TOOLBAR);
        about_button.tooltip_text = "About this application";
        about_button.clicked.connect (() => {show_about (window);});

        var options_buton = new Gtk.Button.from_icon_name ("open-menu", Gtk.IconSize.LARGE_TOOLBAR);
        options_buton.tooltip_text = "Valistic Options";
        options_buton.clicked.connect (() => {opt_windows();});
        
        /*var run_buton = new Gtk.Button.from_icon_name ("media-playback-start", Gtk.IconSize.LARGE_TOOLBAR);
        options_buton.tooltip_text = "Valistic Options";
        options_buton.clicked.connect (() => {opt_windows();});*/

        var new_button = new Gtk.Button.from_icon_name ("list-add", Gtk.IconSize.LARGE_TOOLBAR);
        new_button.tooltip_text = "New Task";
        new_button.clicked.connect (() => { main_stack.set_visible_child_name ("dynamictab");
        });

        home_button = new Gtk.Button.with_label ("Back");
        home_button.get_style_context ().add_class ("back-button");
        home_button.valign = Gtk.Align.CENTER;
        home_button.clicked.connect (() => {
            main_stack.set_visible_child_name ("welcome");
            home_button.hide ();
        });

        save_button = new Gtk.Button.from_icon_name("document-save", Gtk.IconSize.LARGE_TOOLBAR);
        save_button.tooltip_text = "Save File";
        save_button.clicked.connect (() => {
            // save function here
on_save_clicked();
        });

        opt_button = new Gtk.Button.with_label ("New Task");
        opt_button.get_style_context ().add_class ("new-button");
        opt_button.valign = Gtk.Align.CENTER;
        opt_button.clicked.connect (() => {
            opt_windows();
        });

        var avatar = create_avatar ();

        headerbar.pack_end (avatar);
        headerbar.pack_end (about_button);  
        headerbar.pack_end (options_buton);
        headerbar.pack_start (home_button);
        headerbar.pack_start (save_button);
       // headerbar.pack_end (run_buton);
        headerbar.pack_end (new_button);
        window.set_titlebar (headerbar);
    }

    /*private void new_task (){
        var window = new Gtk.Window();
        window.set_title( "Add a new Task" );
        window.set_default_size (680,100);
        window.show_all();
    }*/

    int i;
    private void create_dynamictab () {
        var notebook = new Granite.Widgets.DynamicNotebook ();
        notebook.expand = true;
        for (i = 1; i <= 2; i++) {
            var page = new Gtk.Label ("Page %d".printf (i));
            var tab = new Granite.Widgets.Tab ("Tab %d".printf (i), new ThemedIcon ("mail-mark-important-symbolic"), page);
            notebook.insert_tab (tab, i-1);
        }
        main_stack.add_named (notebook, "dynamictab");

        notebook.new_tab_requested.connect (() => {
            var page = new Gtk.Label ("Page %d".printf (i));
            var tab = new Granite.Widgets.Tab ("Tab %d".printf (i), new ThemedIcon ("mail-mark-important-symbolic"), page);
            notebook.insert_tab (tab, i-1);
            i++;
        });
    }


    private void open_a_file(){
               string? location = get_open_file_location();
        if (location != null) {
            read_file(location);
        }
    }

private string? get_open_file_location() {
        return get_location(new FileChooserDialog(
            "Open File",
            window,
            FileChooserAction.OPEN,
            "Cancel", ResponseType.CANCEL,
            "Open", ResponseType.ACCEPT
        ));
    }

 private string? get_write_file_location() {
        return get_location(new FileChooserDialog(
            "Save File",
            window,
            FileChooserAction.SAVE,
            "Cancel", ResponseType.CANCEL,
            "Save", ResponseType.ACCEPT
        ));
    }


    private void read_file (string file_name){
        if (file_name != "") {
            try {
                            stdout.printf(file_name);
                string txt;
                FileUtils.get_contents(file_name, out txt);
                text_area_view.buffer.text = txt;
                // showtxtarea();
                fe_name = file_name;
                home_button.show ();
                save_button.show ();
                main_stack.set_visible_child_name ("txtarea");            
            } catch (Error e) {
                stderr.printf("error: %s\n", e.message);
            }        
        }else{
            home_button.hide ();
            save_button.hide ();
        }
    }

    private void save_file() {
          try {
            FileUtils.set_contents(fe_name, text_area_view.buffer.text);
            save_button.sensitive = false;
        }
        catch (FileError e) {
            show_file_error(e);
        }
    }


    private void on_save_clicked() {
        if (fe_name == null) {
            fe_name = get_write_file_location();
            if (fe_name == null) {
                return;
            }
        }
        save_file();
    }
    

    private string? get_location(FileChooserDialog file_chooser) {
        string? location = null;
        if (file_chooser.run() == ResponseType.ACCEPT) {
            location = file_chooser.get_filename();
        }
        file_chooser.destroy();
        return location;
    }

    private void show_file_error(FileError error_exception) {
        error(error_exception.message);
    }

    private void error(string message) {
        var dialog = new MessageDialog(
            window,
            DialogFlags.MODAL,
            MessageType.ERROR,
            ButtonsType.CLOSE,
            ""
        );
        dialog.text = message;
        dialog.run();
        dialog.destroy();
    }

    private void showtxtarea(){

            var buffer = new Gtk.TextBuffer(null);
            text_area_view = new Gtk.TextView.with_buffer(buffer);
            text_area_view.set_wrap_mode(Gtk.WrapMode.WORD);
            text_area_view.editable = true;
            text_area_view.cursor_visible = true;
            text_area_view.set_border_window_size(Gtk.TextWindowType.LEFT,14);
            
            //Gdk.RGBA() { red = 1.0, green = 1.0, blue = 0.8, alpha = 1.0 };
            // text_area_view.override_background_color(Gtk.StateFlags.NORMAL,Gdk.RGBA() { red = 19, green = 50, blue = 61, alpha = 0 });

            var scroll = new Gtk.ScrolledWindow(null, null);
            scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC);
            scroll.add(text_area_view);
            
            Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
            uint context_id;
            var statusBar = new Gtk.Statusbar();
            context_id = statusBar.get_context_id("example");
            statusBar.push(context_id, "do something pls");
            //statusBar.show();
            vbox.pack_start(scroll, true, true, 0);
            vbox.pack_start(statusBar, false, false, 0);

            main_stack.add_named (vbox, "txtarea");
    }

    private void opt_windows(){
        var window = new Gtk.Window();
        window.set_title( "Valistic Options" );
        window.set_default_size (600,550);
        Gtk.Stack stack = new Gtk.Stack();
        stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT);

    // giving widgets to stack
        Gtk.Label label = new Gtk.Label("");
        label.set_markup("<big>A label</big>");
        stack.add_titled(label, "label", "A label");

        Gtk.Label label2 = new Gtk.Label("");
        label2.set_markup("<big>Another label</big>");
        stack.add_titled(label2, "label2", "Another label");

        Gtk.Label label3 = new Gtk.Label("");
        label3.set_markup("<big>Third label</big>");
        stack.add_titled(label3, "label3", "third label");


        // add stack(contains widgets) to stackswitcher widget
        Gtk.StackSwitcher stack_switcher = new Gtk.StackSwitcher();
        stack_switcher.halign = Gtk.Align.CENTER;
        stack_switcher.set_stack(stack);

                Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        vbox.pack_start(stack_switcher, false, false, 0);
        vbox.pack_start(stack, false, false, 10);


        window.add(vbox);

        window.show_all();
    }
  
    private Granite.Widgets.Avatar create_avatar () {
        var username = GLib.Environment.get_user_name ();
        var avatar = new Granite.Widgets.Avatar ();
        var iconfile = @"/var/lib/AccountsService/icons/$username";

        avatar.valign = Gtk.Align.CENTER;

        try {
            var pixbuf = new Gdk.Pixbuf.from_file (iconfile);
            avatar.pixbuf = pixbuf.scale_simple (24, 24, Gdk.InterpType.BILINEAR);
            avatar.set_tooltip_text ("Avatar widget: User image found");
        } catch (Error e) {
            avatar.show_default (24);
            avatar.set_tooltip_text ("Avatar widget: User image not found, using fallback");
        }

        return avatar;
    }

    public static int main (string[] args) {
        var application = new Granite.Demo ();
        return application.run (args);
    }
}